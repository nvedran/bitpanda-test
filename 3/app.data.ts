export const VIEWS = [
    {
        key: "coins",
        name: "Crypto Coins"
    },
    {
        key: "metals",
        name: "Metals"
    }
];

export const COLUMNS = [
    {
        key: "id",
        name: "#"
    },
    {
        key: "symbol",
        name: "Symbol"
    },
    {
        key: "name",
        name: "Name"
    },
    {
        key: "avg_price",
        name: "Value"
    }
];
