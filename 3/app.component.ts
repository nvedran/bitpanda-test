import {Component, OnDestroy} from "@angular/core";
import {CryptocoinService} from "../2/service/cryptocoin.service";
import {MetalService} from "../2/service/metal.service";
import {COINS} from "../static_model/cryptocoins";
import {METALS} from "../static_model/metals";
import {COLUMNS, VIEWS} from "./app.data";
import {PageViews, TableColumns} from "./app.interface";
import {Cryptocoin} from "../2/model/cryptocoin";
import {Metal} from "../2/model/metal";
import {Subscription} from "rxjs";

@Component({
    selector: "bitpanda",
    templateUrl: "./app.component.html",
    styleUrls: ["app.component.scss"],
})
export class AppComponent implements OnDestroy {

    // todo: if you want (would be appreciated) you can use the serives from the second task instead
    public coins: Cryptocoin[] = [];
    public metals: Metal[] = [];

    public tableColumns: TableColumns[] = COLUMNS;
    public tableRows: Cryptocoin[] | Metal[] = [];

    public selectedView: string = "coins";
    public pageViews: PageViews[] = VIEWS

    public allSubscriptions: Subscription[] = [];

    public constructor(
        private cryptoService: CryptocoinService,
        private metalService: MetalService
    ) {
        // subscribe for crypto coins data
        const cSub = this.cryptoService.getList().subscribe(coins => {
            this.coins = coins;

            // load table rows if current view is selected
            if(this.isSelected("coins")) {
                this.tableRows = coins;
            }
        });
        this.allSubscriptions.push(cSub);

        // load crypto coins
        this.cryptoService.loadAssets(COINS);

        // subscribe for metal data
        const mSub = this.metalService.getList().subscribe(metals => {
            this.metals = metals;

            // load table rows if current view is selected
            if(this.isSelected("metals")) {
                this.tableRows = metals;
            }
        });
        this.allSubscriptions.push(mSub);

        // load metal data
        this.metalService.loadAssets(METALS);
    }

    setView(selectedView: string): void {
        this.selectedView = selectedView;

        // load data for selected view, "coins" is default view
        switch (selectedView) {
            case 'metals':
                this.tableRows = this.metals;
                break;
            case 'coins':
            default:
                this.tableRows = this.coins;
                break;
        }
    }

    isSelected(view: string): boolean {
        return this.selectedView === view;
    }

    ngOnDestroy(): void {
        for (const sub of this.allSubscriptions) {
            if(!sub.closed) {
                sub.unsubscribe();
            }
        }
    }
}
