export interface TableColumns {
    key: string;
    name: string;
}

export interface PageViews {
    key: string;
    name: string;
}
