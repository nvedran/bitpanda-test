import {Injectable} from "@angular/core";
import {AssetService} from "./interface/asset.service";
import {Metal} from "../model/metal";

@Injectable({
    providedIn: "root"
})
export class MetalService extends AssetService<Metal> {
    constructor() {
        super()
    }
}
