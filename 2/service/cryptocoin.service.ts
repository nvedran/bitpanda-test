import {Injectable} from "@angular/core";
import {AssetService} from "./interface/asset.service";
import {Cryptocoin} from "../model/cryptocoin";

@Injectable({
    providedIn: "root"
})
export class CryptocoinService extends AssetService<Cryptocoin>{

    constructor() {
        super()
    }

}
