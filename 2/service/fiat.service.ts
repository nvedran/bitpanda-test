import {Injectable} from "@angular/core";
import {CurrencyService} from "./interface/currency.service";
import {Fiat} from "../model/fiat";

@Injectable({
    providedIn: "root"
})
export class FiatService extends CurrencyService<Fiat> {

    constructor() {
        super()
    }

    loadFiats(fiats: Fiat[]) {
        this.list = fiats;
    }
}
