import {Service} from "./service";
import {Currency} from "../../model/interface/currency";

export abstract class CurrencyService<C extends Currency> extends Service<C> {

    protected constructor() {
        super();
    }

    // todo implement this method
    public findBySymbol(s: string): C {
        return this.list.find(item => item.symbol === s);
    }

    // todo implement this method
    public findByName(name: string): C {
        return this.list.find(item => item.name === name);
    }

}
