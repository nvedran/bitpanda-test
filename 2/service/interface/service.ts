import {BehaviorSubject} from "rxjs";

import {Model} from "../../model/interface/model";

export abstract class Service<M extends Model> {

    protected _list: BehaviorSubject<M[]> = new BehaviorSubject<M[]>([]);

    public constructor() {
    }

    // todo implement this method
    public destroy(): void {
        this.list = [];
    }

    // todo implement this method
    public updateItem(m: M, id: string = m.id): void {
        const updateIndex = this.getIndexById(id);
        if (updateIndex > -1) {
            const tmpList = [...this.list];
            tmpList[updateIndex] = m;
            this.list = [...tmpList]
        }
    }

    // todo implement this method
    public upsert(m: M): void {
        if (!this.hasItem(m)) {
            this.addItem(m);
        } else {
            console.warn('Item with ID: %s already added', m.id);
        }
    }

    // todo implement this method
    public hasItem(m: M): boolean {
        return this.getIndexById(m.id) > -1;
    }

    // todo implement this method
    public addItem(m: M): void {
        if (!this.hasItem(m)) {
            this.list = [...this.list, m]
        } else {
            console.error('Item with ID: %s already exist', m.id);
        }
    }

    // todo implement this method
    public getItemById(id: string): M {
        return this.list.find(item => item.id === id);
    }

    // todo implement this method
    public getList(): BehaviorSubject<M[]> {
        return this._list;
    }

    // todo implement this method
    public get list(): M[] {
        return [...this._list.getValue()];
    }

    // todo implement this method
    public set list(m: M[]) {
        this._list.next(m);
    }

    // todo implement this method
    public deleteItem(m: M): void {
        const deleteIndex = this.getIndexById(m.id);
        if (deleteIndex > -1) {
            let tmpList = [...this.list];
            tmpList.splice(deleteIndex, 1);
            this.list = [...tmpList]
        }
    }

    private getIndexById(id: string): number {
        return this.list.findIndex(item => item.id === id);
    }
}
