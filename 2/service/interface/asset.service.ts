import {CurrencyService} from "./currency.service";
import {Asset} from "../../model/interface/asset";

export abstract class AssetService<A extends Asset> extends CurrencyService<A> {

    public constructor() {
        super();
    }

    // load data
    loadAssets(assets: A[]) {
        this.list = assets;
    }
}
