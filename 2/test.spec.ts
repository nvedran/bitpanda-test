import { TestBed } from '@angular/core/testing';

import {CryptocoinService} from "./service/cryptocoin.service";
import {FiatService} from "./service/fiat.service";
import {Fiat} from "./model/fiat";
import {Asset} from "./model/interface/asset";
import {Currency} from "./model/interface/currency";
import {MetalService} from "./service/metal.service";
import {COINS} from "../static_model/cryptocoins";
import {METALS} from "../static_model/metals";

describe('Test CryptocoinService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('Load the coins and find one by symbol', () => {
        const service: CryptocoinService = TestBed.get(CryptocoinService);
        service.loadAssets(COINS);
        expect(service.findBySymbol("BTC")).toBeTruthy();
    });
});

describe('Test FiatService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('Upsert a new fiat item and find it by id', () => {
        const service: FiatService = TestBed.get(FiatService);
        const newFiat = new Fiat("1", "EUR", "Euro");
        service.upsert(newFiat);
        expect(service.getItemById("1")).toBeTruthy();
    });

    it('FiatService is not instance of Asset', () => {
        const service: FiatService = TestBed.get(FiatService);
        expect(service instanceof Asset).toBeFalsy();
    });

    it('FiatService is not instance of Currency', () => {
        const service: FiatService = TestBed.get(FiatService);
        expect(service instanceof Currency).toBeFalsy();
    });
});

describe('Test MetalService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('Find an metal item by id', () => {
        const service: MetalService = TestBed.get(MetalService);
        service.loadAssets(METALS);
        expect(service.getItemById("4")).toBeTruthy();
    });

    it('MetalService is not instance of Asset', () => {
        const service: MetalService = TestBed.get(MetalService);
        expect(service instanceof Asset).toBeFalsy();
    });
});
