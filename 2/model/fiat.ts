import {Currency} from "./interface/currency";

export class Fiat extends Currency {

    private precision: number = 2;

    public constructor(id, symbol, name) {
        super();
        this.id = id;
        this.symbol = symbol;
        this.name = name;
    }

    getPrecision(): number {
        return this.precision;
    }

}

