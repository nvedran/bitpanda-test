import {Model} from "./model";

export abstract class Currency extends Model {

    private _name: string = "";

    private _symbol: string = "";

    public abstract getPrecision(): number;

    set name(name: string) {
        this._name = (name && name.trim()) || "";
    }

    get name(): string {
        return this._name;
    }

    set symbol(symbol: string) {
        this._symbol = (symbol && symbol.trim()) || "";
    }

    get symbol(): string {
        return this._symbol;
    }
}
