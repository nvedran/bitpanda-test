export abstract class Model {

    private _id: string = "";

    set id(id: string) {
        this._id = (id && id.trim()) || "";
    }

    get id(): string {
        return this._id;
    }
}
