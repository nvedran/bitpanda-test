import {Currency} from "./currency";

export abstract class Asset extends Currency {

    private _precision_for_fiat_price: number = -1;
    private _avg_price: string = "";

    set precision_for_fiat_price(precision: number) {
        this._precision_for_fiat_price = precision;
    }

    get precision_for_fiat_price(): number {
        return this._precision_for_fiat_price;
    }

    set avg_price(price: string) {
        this._avg_price = (price && price.trim()) || "";
    }

    get avg_price(): string {
        return this._avg_price;
    }

}

