import { Asset } from "./interface/asset";

export class Metal extends Asset {

	private precision_for_metals: number = 3;

	public constructor(id, name, symbol, price) {
		super();
		this.id = id;
		this.name = name;
		this.symbol = symbol;
		this.avg_price = price;
	}

	getPrecision(): number {
		return this.precision_for_metals;
	}

}
