import {Currency} from "./currency";

export abstract class Asset extends Currency {
    protected is_default: boolean = false;
    protected deleted: boolean = false;

    //todo implement me
    public isDefault(): boolean {
        return this.is_default;
    }

    //todo implement me
    public setDefault(b: boolean): void {
        this.is_default = b;
    }
}
