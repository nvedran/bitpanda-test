export abstract class Currency {
    protected id: string = "";
    protected name: string = "";
    protected balance: number = 0;

    //todo implement me
    public getId(): string {
        return this.id;
    }

    //todo implement me
    public setId(id: string): void {
        this.id = (id && id.trim()) || '';
    }

    //todo implement me
    public getName(): string {
        return this.name;
    }

    public getBalance(): number {
        //todo implement me
        return this.balance;
    }

    public setBalance(balance: number): void {
        //todo implement me
        this.balance = balance;
    }

    public reduceBalance(amount: number): void {
        //todo implement me
        this.balance -= amount;
    }

    public addBalance(amount: number): void {
        //todo implement me
        this.balance += amount;
    }

}
