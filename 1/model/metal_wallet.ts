import {Asset} from "./interface/asset";

export class MetalWallet extends Asset {

    private metal_id: string = "";

    public constructor(id, name, balance) {
        super();
        this.setId(id);
        this.setMetalId(id);
        this.setBalance(balance);
        this.name = name;
    }

	//todo implement me
    public getMetalId(): string {
        return this.metal_id;
    }

	//todo implement me
    public setMetalId(id: string): void {
        this.metal_id = (id && id.trim()) || "";
    }
}
