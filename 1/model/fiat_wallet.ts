import {Currency} from "./interface/currency";

export class FiatWallet extends Currency {

    private fiat_id: string = "";

    public constructor(id, name, balance) {
        super();
        this.setId(id);
        this.setFiatId(id);
        this.setBalance(balance);
        this.name = name;
    }

	//todo implement me
    public getFiatId(): string {
        return this.fiat_id;
    }

	//todo implement me
    public setFiatId(id: string): void {
        this.fiat_id = (id && id.trim()) || "";
    }
}
