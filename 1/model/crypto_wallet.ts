import {Asset} from "./interface/asset";

export class CryptoWallet extends Asset {

    private cryptocoin_id: string = "";

    public constructor(id, name, balance) {
        super();
        this.setId(id);
        this.setCryptocoinId(id);
        this.setBalance(balance);
        this.name = name;
    }

	//todo implement me
    public getCryptocoinId(): string {
        return this.cryptocoin_id;
    }

	//todo implement me
    public setCryptocoinId(id: string): void {
        this.cryptocoin_id = (id && id.trim()) || "";
    }
}
